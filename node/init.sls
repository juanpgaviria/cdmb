{% if grains['osarch'].startswith('i386') %}
node_src: node-v0.10.26-linux-x86.tar.gz
{% elif grains['osarch'].startswith('amd64') %}
node_src: node-v0.10.26-linux-x64.tar.gz
{% else %}
node_src: node-v0.10.26-linux-x86.tar.gz
{% endif %}
