base:
  '*':
    - vim
  'roles:webserver':
    - match: grain
    - node
qa:    
  'G@environment:staging and G@roles:webserver':
    - match: compound
    - webapp
    - nginx
  
  'G@environment:staging and G@roles:database':
    - match: compound
    - mongodb
    - redis
prod:
  'G@environment:production and G@roles:webserver':
    - match: compound
    - webapp
    - nginx

