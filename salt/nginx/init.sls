nginx-1_4:
  pkgrepo.managed:
    - humanname: MongoDB 2.4
    - name: deb http://nginx.org/packages/ubuntu/ precise nginx
    - file: /etc/apt/sources.list.d/mongodb.list
    - keyid: ABF5BD827BD9BF62
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - pkg: mongodb-10gen

  pkg.latest:
    - name: nginx
    - refresh: True

nginx:
  pkg:
    - installed
  service:
    - running
    - watch:
      - pkg: nginx
      - file: /etc/nginx/nginx.conf
      - file: /etc/nginx/sites-enabled

/etc/nginx/nginx.conf:
  file.managed:
    - source: salt://nginx/conf/nginx.conf
      mode: 644

/etc/nginx/sites-enabled:
  file.recurse:
    - source: salt://nginx/conf/sites-enabled
    - include_empty: True
