base:
  pkgrepo.managed:
    - humanname: MongoDB 2.4
    - name: deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen
    - file: /etc/apt/sources.list.d/mongodb.list
    - keyid: 7F0CEB10
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - pkg: mongodb-10gen

  pkg.latest:
    - name: mongodb-10gen
    - refresh: True

mongodb-10gen:
  pkg.latest

mongodb:
  service:
    - running
    - require:
      - pkg: mongodb-10gen
    - watch:
      - file: /etc/mongodb.conf

/etc/mongodb.conf:
  file.managed:
    - source: salt://mongodb/conf/mongodb.conf
    - mode: 644

