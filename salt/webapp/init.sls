webapp:
  service:
    - running
    - watch:
      - file: /etc/init.d/webapp
      - file: /var/www/webapp

/etc/init.d/webapp:
  file.managed:
    - source: salt://webapp/conf/webapp.init.d
    - mode: 755
    
/var/www/webapp:
  file.recurse:
    - source: salt://webapp/app
    - include_empty: True