/usr/local/src/{{ pillar['node_src'] }}:
  file.managed:
    - source: salt://node/src/{{ pillar['node_src'] }}
    - makedirs: true

extract_node:
  cmd.run:
    - name: "cd /usr/local && tar --strip-components 1 -xzf /usr/local/src/{{ pillar['node_src'] }}"
    - cwd: /usr/local/src/
    - unless: test -e /usr/local/bin/node
    - require:
      - file: /usr/local/src/{{ pillar['node_src'] }}

/usr/local/bin/node:
  file.managed:
    - require:
      - cmd: extract_node

/usr/local/bin/npm:
  file.managed:
    - require:
      - cmd: extract_node
